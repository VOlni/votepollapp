from django.urls import path

from . import views


# new way with generics
app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),     # ex: /polls/5/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),    # ex: /polls/5/results/
    path('<int:question_id>/vote/', views.vote, name='vote'),            # ex: /polls/5/vote/
]

"""
The question_id=34 part comes from <int:question_id>. 
Using angle brackets “captures” part of the URL and sends it as a keyword argument to the view function. 
The :question_id> part of the string defines the name that will be used to identify the matched pattern, 
and the <int: part is a converter that determines what patterns should match this part of the URL path.
"""
# old ways, hardcoded views
# urlpatterns = [
#     path('', views.index, name='index'),
#     path('<int:question_id>/', views.detail, name='detail'),     # ex: /polls/5/
#     path('<int:question_id>/results/', views.results, name='results'),    # ex: /polls/5/results/
#     path('<int:question_id>/vote/', views.vote, name='vote'),            # ex: /polls/5/vote/
# ]